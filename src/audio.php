<?php

namespace MerageAudio\Audio;


class audio
{
    /**
     * 合并音频
     * @param $audios array 音频文件
     * @param $merge_new_file string 存放文件目录
     * return array
     * */
    public static function mergeAudio($audios,$merge_new_file){
        if(!$audios) return "";
        $audios_str = implode('|',$audios);
        $file_name =time().mt_rand(111,999).'.mp3';
        $merge_cmd = ' ffmpeg -i "concat:'.$audios_str.'" -acodec copy '.$merge_new_file.$file_name;
        exec($merge_cmd, $out, $status);
        if($status != 0 && !file_exists($merge_new_file.$file_name)) return false;
        return $merge_new_file.$file_name;
    }


    /**
     * 加入背景音乐
     * @param $audio string 音频文件路径(全路径)
     * @param $bj string 背景音频路径(全路径)
     * @param $syn_file_path string 保存路径
     * return array
     * */
    public static function addBgAduio($audio,$bj,$syn_file_path){
        if(!file_exists($audio)) return '';
        //转换后的路径
        $file_name =time().mt_rand(111,999).'.mp3';
        $bg_cmd ='ffmpeg -i "'.$audio.'" -i '.$bj.'  -filter_complex "[0:a]volume=2[a1];[1:a]volume=0.5[a2];[a1][a2]amix=inputs=2:duration=first:dropout_transition=1[aout]" -map "[aout]"  -y '.$syn_file_path.$file_name;
        exec($bg_cmd, $out, $status);
        if($status != 0) return false;
        return $syn_file_path.$file_name;
    }


}
